#ifndef LEAKS_FILE_LINE_NEW_H_
#define LEAKS_FILE_LINE_NEW_H_

#include "../src/leak_op_new.h"

#define new new(__FILE__, __LINE__)

#endif
