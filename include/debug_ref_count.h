#ifndef LEAKS_DEBUG_REF_COUNT_H_
#define LEAKS_DEBUG_REF_COUNT_H_

namespace debug_ref_count
{

void init(void* a_addr);
void grab(void* a_addr, const char* a_file_name, unsigned int a_line);
void drop(void* a_addr, const char* a_file_name, unsigned int a_line);
void end(void* a_addr);
    
}

#endif
