#ifndef LEAKS_FILE_LINE_REF_H_
#define LEAKS_FILE_LINE_REF_H_

#define grab() grab(__FILE__, __LINE__)
#define drop() drop(__FILE__, __LINE__)

#endif
