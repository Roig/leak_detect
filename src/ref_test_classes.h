#ifndef LEAKS_REF_TEST_CLASSES_H_
#define LEAKS_REF_TEST_CLASSES_H_

#ifdef DEBUG_REF_COUNTERS
#include "../include/debug_ref_count.h"
#endif

class ref_counter
{
public:
    ref_counter() : m_counter(1)
    {
#ifdef DEBUG_REF_COUNTERS
	debug_ref_count::init(this);
#endif	
    }

    ~ref_counter()
    {
#ifdef DEBUG_REF_COUNTERS	
	debug_ref_count::end(this);
#endif	
    }
    
    void grab()
    {
	++m_counter;
    }

    void grab(const char* a_file_name, unsigned int a_line)
    {
#ifdef DEBUG_REF_COUNTERS	
	debug_ref_count::grab(this, a_file_name, a_line);
#endif
	grab();
    }

    void drop()
    {
	--m_counter;

	if (!m_counter)
	    delete this;
    }

    void drop(const char* a_file_name, unsigned int a_line)
    {
#ifdef DEBUG_REF_COUNTERS
	debug_ref_count::drop(this, a_file_name, a_line);
#endif	
	drop();
    }
	    
private:
    int m_counter;
};

class counted : public ref_counter
{    
};

#ifdef DEBUG_REF_COUNTERS
// redefine grab y drop para obtener nombre de fichero y linea
#include "../include/file_line_ref.h"
#endif

#endif
