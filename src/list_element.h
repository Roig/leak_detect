#ifndef LEAKS_LIST_ELEMENT_H_
#define LEAKS_LIST_ELEMENT_H_

#define KEY_MAX_SIZE 100

namespace leaks
{

class list_element
{
public:
    void init() { }
    
    void* m_key;
    void* m_element;
    list_element* m_next;
};

class str_list_element
{
public:
    void init() { m_key = m_key_buf; }
    
    char* m_key;
    char m_key_buf[KEY_MAX_SIZE];
    void* m_element;
    str_list_element* m_next;
};

}

#endif
