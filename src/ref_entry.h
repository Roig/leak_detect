#ifndef LEAKS_REF_ENTRY_H_
#define LEAKS_REF_ENTRY_H_

#include "hash_table.h"
#include "allocation.h"

#define ENTRY_TABLE_SIZE 500

namespace leaks
{

class ref_entry
{
public:
    void init();
    void grab(const char* a_file_name, unsigned int a_line);
    void drop(const char* a_file_name, unsigned int a_line);
    void dump_bad_count();

    static void create_key(char* a_dest, const char* a_file_name, unsigned int a_line);
    
    allocation m_ctor;
    hash_table m_grabs;
    hash_table m_drops;
    int m_counter;
};
    
}

#endif
