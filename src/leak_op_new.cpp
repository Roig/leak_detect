#include "leak_op_new.h"
#include "leak_tracker.h"
#include <cstdlib>

using namespace std;
using namespace leaks;

leak_tracker& tracker = leak_tracker::get_instance();

void* operator new(std::size_t a_size, const char* a_file_name, unsigned int a_line) throw(bad_alloc)
{    
    void* ptr = malloc(a_size);

    if (!ptr)
	throw;

    tracker.add_entry(ptr, a_size, false, a_file_name, a_line);
    
    return ptr;
}

void* operator new(std::size_t a_size, const std::nothrow_t& a_nothrow_constant, const char* a_file_name, unsigned int a_line) throw()
{
    void* ptr = malloc(a_size);

    tracker.add_entry(ptr, a_size, false, a_file_name, a_line);
    
    return ptr;    
}

void* operator new[](std::size_t a_size, const char* a_file_name, unsigned int a_line) throw(bad_alloc)
{
    void* ptr = malloc(a_size);

    if (!ptr)
	throw;

    tracker.add_entry(ptr, a_size, true, a_file_name, a_line);
    
    return ptr;    
}

void* operator new[](std::size_t a_size, const std::nothrow_t& a_nothrow_constant, const char* a_file_name, unsigned int a_line) throw()
{
    void* ptr = malloc(a_size);

    tracker.add_entry(ptr, a_size, true, a_file_name, a_line);
    
    return ptr;
}

void* operator new(std::size_t a_size) throw(bad_alloc)
{
    void* ptr = malloc(a_size);

    if (!ptr)
	throw;

    tracker.add_entry(ptr, a_size, false);

    return ptr;
}

void* operator new(std::size_t a_size, const std::nothrow_t& a_nothrow_constant) throw()
{
    void* ptr = malloc(a_size);

    tracker.add_entry(ptr, a_size, false);

    return ptr;    
}

void* operator new[](std::size_t a_size) throw(bad_alloc)
{
    void* ptr = malloc(a_size);

    if (!ptr)
	throw;

    tracker.add_entry(ptr, a_size, true);

    return ptr;    
}

void* operator new[](std::size_t a_size, const std::nothrow_t& a_nothrow_constant) throw()
{
    void* ptr = malloc(a_size);

    tracker.add_entry(ptr, a_size, true);

    return ptr;    
}

void operator delete(void* a_ptr) throw()
{
    tracker.remove_entry(a_ptr, false);

    free(a_ptr);
}

void operator delete(void* a_ptr, const std::nothrow_t& a_nothrow_constant) throw()
{
    tracker.remove_entry(a_ptr, false);

    free(a_ptr);
}

void operator delete[](void* a_ptr) throw()
{
    tracker.remove_entry(a_ptr, true);

    free(a_ptr);
}

void operator delete[](void* a_ptr, const std::nothrow_t& a_nothrow_constant) throw()
{
    tracker.remove_entry(a_ptr, true);

    free(a_ptr);
}
