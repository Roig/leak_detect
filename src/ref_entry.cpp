#include "ref_entry.h"
#include <cstdlib>
#include <cstring>
#include <cstdio>

#define COLOR_CTOR		"\033[00;46m\033[01;37m"
#define COLOR_GRABS		"\033[00;41m\033[01;37m"
#define COLOR_DROPS		"\033[01;42m\033[01;37m"
#define COLOR_RESET		"\033[0m"

using namespace leaks;

void ref_entry::init()
{
    m_ctor.init();
    m_counter = 1;

    m_grabs.init(ENTRY_TABLE_SIZE);
    m_drops.init(ENTRY_TABLE_SIZE);
}

void ref_entry::grab(const char* a_file_name, unsigned int a_line)
{    
    allocation* n_alloc = (allocation*)malloc(sizeof(allocation));
    n_alloc->init();

    if (a_file_name)
    {
	int str_size = strlen(a_file_name) + 1;
	n_alloc->m_file_name = (char*)malloc(str_size);
	memcpy(n_alloc->m_file_name, a_file_name, str_size);

	n_alloc->m_line = a_line;	
    }

    char key[KEY_MAX_SIZE] = {0};
    create_key(key, a_file_name, a_line);

    m_grabs.insert(key, n_alloc);
    
    ++m_counter;
}

void ref_entry::drop(const char* a_file_name, unsigned int a_line)
{
    allocation* n_alloc = (allocation*)malloc(sizeof(allocation));
    n_alloc->init();

    if (a_file_name)
    {
	int str_size = strlen(a_file_name) + 1;
	n_alloc->m_file_name = (char*)malloc(str_size);
	memcpy(n_alloc->m_file_name, a_file_name, str_size);

	n_alloc->m_line = a_line;	
    }

    char key[KEY_MAX_SIZE] = {0};
    create_key(key, a_file_name, a_line);

    m_drops.insert(key, n_alloc);

    --m_counter;
}

void ref_entry::dump_bad_count()
{
    printf("remaining references: %d\n", m_counter);

    printf("%screation backtrace: ====================================================================%s\n", COLOR_CTOR, COLOR_RESET);
    m_ctor.dump_backtrace();
    m_ctor.clean();

    printf("\n%sgrabs(%d + 1): >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%s\n", COLOR_GRABS, m_grabs.get_count(),
	COLOR_RESET);
    for (unsigned int index = 0; index < m_grabs.get_size(); ++index)
    {
	linked_list<str_list_element>* list = m_grabs.get_list(index);
	if (list)
	{
	    while(allocation* l_alloc = (allocation*)list->pop_front())
	    {
		l_alloc->dump_backtrace();
		l_alloc->clean();
		free(l_alloc);
	    }
	}
    }

    printf("\n%sdrops(%d): <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s\n", COLOR_DROPS, m_drops.get_count(),
	COLOR_RESET);
    for (unsigned int index = 0; index < m_grabs.get_size(); ++index)
    {
	linked_list<str_list_element>* list = m_drops.get_list(index);
	if (list)
	{
	    while(allocation* l_alloc = (allocation*)list->pop_front())
	    {
		l_alloc->dump_backtrace();
		l_alloc->clean();
		free(l_alloc);	
	    }	    
	}	
    }

    m_grabs.clean();
    m_drops.clean();
}

void ref_entry::create_key(char* a_dest, const char* a_file_name, unsigned int a_line)
{
    snprintf(a_dest, KEY_MAX_SIZE, "%s:%d",  a_file_name, a_line);
}
