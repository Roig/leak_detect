#include "ref_tracker.h"
#include "ref_entry.h"
#include <cstring>
#include <cstdlib>
#include <cstdio>

using namespace leaks;

ref_tracker::ref_tracker()
{
    m_table.init(REF_TABLE_SIZE);
}

ref_tracker::~ref_tracker()
{
    if (unsigned int count = m_table.get_count())
	printf("\n--------------------------- bad reference counting(%d) detected ----------------------------\n", count);

    for (unsigned int index = 0; index < m_table.get_size(); ++index)
    {
	if (linked_list<str_list_element>* list = m_table.get_list(index))
	{
	    while(ref_entry* l_entry = (ref_entry*)list->pop_front())
	    {
		l_entry->dump_bad_count();
		printf("\n\n----------------------------------------------------------------------------------------\n");

		free(l_entry);
	    }
	}	
    }

    m_table.clean();
}

void ref_tracker::add_entry(void* a_ptr)
{
    ref_entry* n_entry = (ref_entry*)malloc(sizeof(ref_entry));
    n_entry->init();

    char str_key[REF_KEY_SIZE] = {0};
    snprintf(str_key, REF_KEY_SIZE, "%p", a_ptr);
    m_table.insert(str_key, n_entry);
}

void ref_tracker::grab(void* a_ptr, const char* a_file_name, unsigned int a_line)
{
    char str_key[REF_KEY_SIZE] = {0};
    snprintf(str_key, REF_KEY_SIZE, "%p", a_ptr);

    ref_entry* l_entry = (ref_entry*)m_table.find(str_key);
    if (!l_entry)
	return;

    l_entry->grab(a_file_name, a_line);
}

void ref_tracker::drop(void* a_ptr, const char* a_file_name, unsigned int a_line)
{
    char str_key[REF_KEY_SIZE] = {0};
    snprintf(str_key, REF_KEY_SIZE, "%p", a_ptr);
    
    ref_entry* l_entry = (ref_entry*)m_table.find(str_key);
    if (!l_entry)
	return;

    l_entry->drop(a_file_name, a_line);
}

void ref_tracker::remove_entry(void* a_ptr)
{
    char str_key[REF_KEY_SIZE] = {0};
    snprintf(str_key, REF_KEY_SIZE, "%p", a_ptr);
    
    ref_entry* l_entry = (ref_entry*)m_table.remove(str_key);
    if (l_entry)
	free(l_entry);
}
