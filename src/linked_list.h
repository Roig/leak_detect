#ifndef LEAKS_LINKED_LIST_H_
#define LEAKS_LINKED_LIST_H_

#include "list_element.h"

namespace leaks
{

template <typename t_list_entry = list_element>
class linked_list
{
public:
    linked_list();
    ~linked_list();

    void init();
    void clean();
    void add(void* a_key, void* a_element);
    void* remove(void* a_key);
    void* pop_front();
    void* get(void* a_key);
    
    unsigned int get_size();

protected:
    template <typename t_key>
    void write_key(t_key* a_entry, void* a_key);

    template <typename t_key>
    bool compare_key(t_key* a_left, void* a_right);
    
private:
    t_list_entry* m_first;
    unsigned int m_size;
};

}

#endif
