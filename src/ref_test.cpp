#define DEBUG_REF_COUNTERS
#include "ref_test_classes.h"

void testfunc5(counted* cnt)
{
    cnt->grab();
}

void testfunc4(counted* cnt)
{
    cnt->grab();
    cnt->drop();
    
    testfunc5(cnt);
}

void testfunc3(counted* cnt)
{
    cnt->drop();

    testfunc4(cnt);
}

void testfunc2(counted* cnt)
{
    cnt->grab();
    
    testfunc3(cnt);
}

void testfunc1()
{
    counted* cnt = new counted;
    counted* cnt2 = new counted;
    
    testfunc2(cnt);

    cnt2->drop();
    
    // normalmente suponemos que este seria el ultimo drop
    cnt->drop();
}

int main(int argc, char* argv[])
{
    testfunc1();

    return 0;
}

