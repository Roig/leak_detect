#ifndef SINGLETON_H_
#define SINGLETON_H_

template <typename type>
class singleton
{
public:
	static type& get_instance()
	{
	    static type m_instance;
	    return m_instance;
	}

protected:
	singleton() { }
	~singleton() { }

private:
	singleton(singleton const&) { }
	void operator=(singleton const&) { }
};

#endif
