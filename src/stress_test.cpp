#include <vector>
#include <cstdlib>
#include <ctime>
#include <cstdio>

#define MAX_ALLOCATIONS 1000000

int main(int argc, char* argv[])
{
    std::vector<int*> ptrs;
    ptrs.reserve(MAX_ALLOCATIONS);

    for (unsigned int i = 0; i < MAX_ALLOCATIONS; ++i)
	ptrs.push_back(new int(rand()%2345243));
    
//     while(!ptrs.empty())
//     {
// 	unsigned int index = rand() % ptrs.size();
// 	delete ptrs[index];
// 	ptrs.erase(ptrs.begin()+index);
// //	usleep(rand()%10000);
//     }

    for (unsigned int index = 0; index < ptrs.size(); ++index)
    	delete ptrs[index];
    
    return 0;
}
