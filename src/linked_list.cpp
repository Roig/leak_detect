#include "linked_list.h"
#include <cstdlib>
#include <cstring>

namespace leaks
{

template <typename t_list_entry>
void linked_list<t_list_entry>::init()
{
    m_first = 0;
    m_size = 0;
}

template <typename t_list_entry>
void linked_list<t_list_entry>::clean()
{
    while(pop_front());    
}

template <typename t_list_entry>
void linked_list<t_list_entry>::add(void* a_key, void* a_element)
{
    t_list_entry* n_entry = (t_list_entry*)malloc(sizeof(t_list_entry));
    n_entry->init();
    write_key(&n_entry->m_key, a_key);
    n_entry->m_element = a_element;

    n_entry->m_next = m_first;
    m_first = n_entry;

    ++m_size;
}

// removes only first match
template <typename t_list_entry>
void* linked_list<t_list_entry>::remove(void* a_key)
{
    if (!m_first)
	return NULL;
    
    t_list_entry* next = m_first;
    t_list_entry* prev = NULL;

    while(next != NULL && !compare_key(next->m_key, a_key))
    {	
	prev = next;	
	next = next->m_next;
    }

    if (!next)
	return NULL;
    
    if (prev)
	prev->m_next = next->m_next;

    if (m_first == next)
	m_first = next->m_next;
    
    void* element = next->m_element;
    free(next);

    --m_size;

    return element;
}

template <typename t_list_entry>
unsigned int linked_list<t_list_entry>::get_size()
{
    return m_size;
}

template <typename t_list_entry>
void* linked_list<t_list_entry>::pop_front()
{
    if (!m_first)
	return NULL;

    t_list_entry* front = m_first;
    m_first = m_first->m_next;

    void* element = front->m_element;
    free(front);
    
    return element;
}

template <typename t_list_entry>
void* linked_list<t_list_entry>::get(void* a_key)
{
    if (!m_first)
	return NULL;
    
    t_list_entry* next = m_first;
    while(next != NULL && !compare_key(next->m_key, a_key))
	next = next->m_next;

    return next? next->m_element : NULL;
}

template <>
template <>
void linked_list<str_list_element>::write_key(char** a_dst, void* a_orig)
{
    strcpy(*a_dst, (char*)a_orig);
}

template <>
template <>
void linked_list<list_element>::write_key(void** a_dst, void* a_orig)
{
    *a_dst = a_orig;
}

template <>
template <>
bool linked_list<str_list_element>::compare_key(char* a_left, void* a_right)
{
    return !strcmp(a_left, (char*)a_right);
}

template <>
template <>
bool linked_list<list_element>::compare_key(void* a_left, void* a_right)
{
    return a_left == a_right;
}

template class linked_list<list_element>;
template class linked_list<str_list_element>;
    
}

