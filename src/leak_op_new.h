#ifndef LEAK_OP_NEW_H_
#define LEAK_OP_NEW_H_

#include <cstddef>
#include <stdexcept>

// overloaded
void* operator new(std::size_t a_size, const char* a_file_name,  unsigned int a_line) throw(std::bad_alloc);
void* operator new(std::size_t a_size, const std::nothrow_t& a_nothrow_constant, const char* a_file_name, unsigned int a_line) throw();
void* operator new[](std::size_t a_size, const char* a_file_name, unsigned int a_line) throw(std::bad_alloc);
void* operator new[](std::size_t a_size, const std::nothrow_t& a_nothrow_constant, const char* a_file_name, unsigned int a_line) throw();

// standard
void* operator new(std::size_t a_size) throw(std::bad_alloc);
void* operator new(std::size_t a_size, const std::nothrow_t& a_nothrow_constant) throw();
void* operator new[](std::size_t a_size) throw(std::bad_alloc);
void* operator new[](std::size_t a_size, const std::nothrow_t& a_nothrow_constant) throw();
void operator delete(void* a_ptr) throw();
void operator delete(void* a_ptr, const std::nothrow_t& a_nothrow_constant) throw();
void operator delete[](void* a_ptr) throw();
void operator delete[](void* a_ptr, const std::nothrow_t& a_nothrow_constant) throw();

#endif
