#include "leak_allocation.h"
#include <execinfo.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <cxxabi.h>

using namespace leaks;

void leak_allocation::init()
{
    allocation::init();
    
    m_array = false;
}
