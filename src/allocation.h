#ifndef LEAKS_ALLOCATION_H_
#define LEAKS_ALLOCATION_H_

#define ALLOCATION_MAX_FRAMES	20
#define EXE_PATH_SIZE		150
#define PIPE_BUFFER_SIZE	150
#define OUT_STRING_SIZE		125
#define COLOR_BLUE		"\033[01;34m"
#define COLOR_RED		"\033[01;31m"
#define COLOR_RESET		"\033[0m"

namespace leaks
{

class allocation
{
public:
    void init();
    char** get_symbols();
    void clean();
    void dump_backtrace();

    void* m_trace[ALLOCATION_MAX_FRAMES];
    char** m_symbols;
    int m_trace_size;
    char* m_file_name;
    unsigned int m_line;
    
private:
    char* demangle(char* a_in);
    char* get_file_line_info(unsigned int a_index);
};
    
}

#endif
