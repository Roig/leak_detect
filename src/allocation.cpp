#include "allocation.h"
#include <execinfo.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <cxxabi.h>
#include <dlfcn.h>

using namespace leaks;

void allocation::init()
{
    m_trace_size = backtrace(m_trace, ALLOCATION_MAX_FRAMES);
    m_symbols = NULL;
    m_file_name = NULL;
    m_line = 0;
}

char** allocation::get_symbols()
{
    if (!m_symbols)
	m_symbols = backtrace_symbols(m_trace, m_trace_size);

    return m_symbols;
}

void allocation::clean()
{
    if (m_symbols)
	free(m_symbols);

    if (m_file_name)
	free(m_file_name);
}

void allocation::dump_backtrace()
{
    get_symbols();

    if (m_file_name)
	printf("\t[ %s%s:%d%s ]\n", COLOR_BLUE, m_file_name, m_line, COLOR_RESET);

    // don't print the first 4 and last 2 frames, they are always the same
    for (int index = 4; index < m_trace_size - 2; ++index)
    {
	char* func_name = demangle(m_symbols[index]);

	if (func_name)
	{
	    printf("%s%s%s\n", COLOR_RED, func_name, COLOR_RESET);
	    free(func_name);
	}
    }

    printf("%s", COLOR_RESET);
    for (int index = 4; index < m_trace_size - 2; ++index)
    {
	char* file_line = get_file_line_info(index);

	if (file_line)
	{
	  printf("     %s\n", file_line);	    
	  free(file_line);
	}
    }
}

char* allocation::demangle(char* a_in)
{
    char* begin = a_in;
    char* end = NULL;

    while(*begin != '(' && *begin != '\0')
	++begin;

    end = ++begin;
    
    while(*end != '+' && *end != ')' && *end != '\0')
	++end;

    int status;
    size_t len = (size_t)(end - begin);

    char* mangled = (char*)malloc(len + 1);
    memset(mangled, 0, len + 1);
    memcpy(mangled, begin, len);
    
    char* demangled = abi::__cxa_demangle(mangled, NULL, &len, &status);

    if (!demangled)
	demangled = mangled;
    else
      free(mangled);
    
    return demangled;
}

char* allocation::get_file_line_info(unsigned int a_index)
{
    char executable[EXE_PATH_SIZE] = {0};
    if (readlink("/proc/self/exe", executable, EXE_PATH_SIZE) == -1)
	return NULL;
    
    char* end = m_symbols[a_index];
    while(*end != '(' && *end != '\0')
	++end;

    char* curr_dir = get_current_dir_name();
    size_t len = (size_t)(--end - m_symbols[a_index]) + strlen(curr_dir);
    char* target = (char*)calloc(len + 1, sizeof(char));
    strcpy(target, curr_dir);
    memcpy(target + strlen(target), m_symbols[a_index] + 1, (end - m_symbols[a_index]));
    free(curr_dir);

    void* target_addr = m_trace[a_index];
    if (strcmp(target, executable))
    {	
	Dl_info info;
	dladdr(m_trace[a_index], &info);
	if (!info.dli_sname && !info.dli_saddr)
	{
	    free(target);
	    return NULL;
	}

	target_addr = (void*)((char*)target_addr - (char*)info.dli_fbase);
	strcpy(target, info.dli_fname);
    }
    
    char address[20];
    snprintf(address, sizeof(address), "%p", target_addr);
    
    char* pipe_buffer = (char*)calloc(PIPE_BUFFER_SIZE, sizeof(char));
    // still don't know how to read dwarf2 info... I have a life
    snprintf(pipe_buffer, PIPE_BUFFER_SIZE, "addr2line -e %s %s", target, address);
    free(target);

    FILE* pipe_in = popen(pipe_buffer, "r");
    fgets(pipe_buffer, PIPE_BUFFER_SIZE, pipe_in);

    int status = pclose(pipe_in);    
    if(WIFEXITED(status) && WEXITSTATUS(status))
	pipe_buffer[0] = '\0';
    else if (pipe_buffer[strlen(pipe_buffer) - 1] == '\n')
	pipe_buffer[strlen(pipe_buffer) - 1] = '\0';

    return pipe_buffer;
}

