#ifndef LEAKS_LEAK_TRACKER_H_
#define LEAKS_LEAK_TRACKER_H_

#include "singleton.h"
#include "allocation.h"
#include "hash_table.h"
#include <cstddef>

#define LEAK_TABLE_SIZE 10000
#define LEAK_KEY_SIZE 20

namespace leaks
{

class leak_tracker : public singleton<leak_tracker>
{
public:
    void add_entry(void* a_ptr, size_t a_size, bool a_array, const char* a_file_name = 0, unsigned int a_line = 0);
    void remove_entry(void* a_ptr, bool a_array);
    
private:
    friend class singleton<leak_tracker>;
    leak_tracker();
    ~leak_tracker();
    
    leak_tracker(const leak_tracker& a_other) { }

    hash_table m_table;
};
    
}

#endif
