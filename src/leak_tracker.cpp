#include "leak_tracker.h"
#include "leak_allocation.h"
#include <cstring>
#include <cstdlib>
#include <cstdio>

using namespace leaks;

leak_tracker::leak_tracker()
{
    m_table.init(LEAK_TABLE_SIZE);
}

leak_tracker::~leak_tracker()
{
    if (unsigned int count = m_table.get_count())
	printf("\n--------------------------- leaks(%d) detected ----------------------------\n", count);

    for (unsigned int index = 0; index < m_table.get_size(); ++index)
    {
	if (linked_list<str_list_element>* list = m_table.get_list(index))
	{
	    while(leak_allocation* l_alloc = (leak_allocation*)list->pop_front())
	    {
		l_alloc->dump_backtrace();
		printf("\n\n-----------------------------------------------------------------------\n");

		l_alloc->clean();
		free(l_alloc);	
	    }
	}
   }

    m_table.clean();
}

void leak_tracker::add_entry(void* a_ptr, size_t a_size, bool a_array, const char* a_file_name, unsigned int a_line)
{
    leak_allocation* n_alloc = (leak_allocation*)malloc(sizeof(leak_allocation));
    n_alloc->init();
    n_alloc->m_array = a_array;

    if (a_file_name)
    {
	int str_size = strlen(a_file_name) + 1;
	n_alloc->m_file_name = (char*)malloc(str_size);
	memcpy(n_alloc->m_file_name, a_file_name, str_size);

	n_alloc->m_line = a_line;
    }

    char str_key[LEAK_KEY_SIZE] = {0};
    snprintf(str_key, LEAK_KEY_SIZE, "%p", a_ptr);
    m_table.insert(str_key, n_alloc);
}

void leak_tracker::remove_entry(void* a_ptr, bool a_array)
{
//@TODO avisar de equivocacion delete/delete[]
    char str_key[LEAK_KEY_SIZE] = {0};
    snprintf(str_key, LEAK_KEY_SIZE, "%p", a_ptr);    
    leak_allocation* l_alloc = (leak_allocation*)m_table.remove(str_key);
    if (l_alloc)
	free(l_alloc);    
}

