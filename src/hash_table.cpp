#include "hash_table.h"
#include <cstdlib>

using namespace leaks;

void hash_table::init(unsigned int a_size)
{
    m_table = (linked_list<str_list_element>**)malloc(sizeof(linked_list<str_list_element>*) * a_size);
    for (unsigned int index = 0; index < a_size; ++index)
	m_table[index] = NULL;
    m_size = a_size;
    m_count = 0;
}

unsigned int hash_table::hash(char* a_str)
{
    unsigned int hashval = 0;
    for(; *a_str!='\0'; ++a_str)
	hashval = *a_str + (hashval << 5) - hashval;

    return hashval % m_size;
}

void* hash_table::find(char* a_key)
{
    linked_list<str_list_element>* list = m_table[hash(a_key)];
    if (list)
    	return list->get(a_key);

    return NULL;
}

void hash_table::insert(char* a_key, void* a_element)
{
    linked_list<str_list_element>*& list = m_table[hash(a_key)];
    if (!list)
    {
        list = (linked_list<str_list_element>*)malloc(sizeof(linked_list<str_list_element>));
	list->init();
    }

    list->add(a_key, a_element);

    ++m_count;
}

void* hash_table::remove(char* a_key)
{
    linked_list<str_list_element>* list = m_table[hash(a_key)];
    if (list)
    {
	void* retval = list->remove(a_key);
	if (retval)
	    --m_count;
	
	return retval;	
    }

    return NULL;
}

void hash_table::clean()
{
    for (unsigned int index = 0; index < m_size; ++index)
    {
	linked_list<str_list_element>* list = m_table[index];
	if (list)
	{
	    list->clean();
	    free(list);
	}
    }

    free(m_table);
    
    m_size = 0;
    m_count = 0;
}

unsigned int hash_table::get_size()
{
    return m_size;
}

linked_list<str_list_element>* hash_table::get_list(unsigned int a_index)
{
    if (a_index < m_size)
	return m_table[a_index];

    return NULL;
}

unsigned int hash_table::get_count()
{
    return m_count;
}
