#include "../include/debug_ref_count.h"
#include "ref_tracker.h"

namespace debug_ref_count
{

leaks::ref_tracker& tracker = leaks::ref_tracker::get_instance();
    
void init(void* a_addr)
{
    tracker.add_entry(a_addr);
}
    
void grab(void* a_addr, const char* a_file_name, unsigned int a_line)
{
    tracker.grab(a_addr, a_file_name, a_line);
}
    
void drop(void* a_addr, const char* a_file_name, unsigned int a_line)
{
    tracker.drop(a_addr, a_file_name, a_line);
}
    
void end(void* a_addr)
{
    tracker.remove_entry(a_addr);
}

}
