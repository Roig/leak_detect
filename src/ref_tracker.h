#ifndef LEAKS_REF_TRACKER_H_
#define LEAKS_REF_TRACKER_H_

#include "singleton.h"
#include "allocation.h"
#include "hash_table.h"

#define REF_TABLE_SIZE 500
#define REF_KEY_SIZE 20

namespace leaks
{

class ref_tracker : public singleton<ref_tracker>
{
public:
    void add_entry(void* a_ptr);
    void grab(void* a_ptr, const char* a_file_name = 0, unsigned int a_line = 0);
    void drop(void* a_ptr, const char* a_file_name = 0, unsigned int a_line = 0);
    void remove_entry(void* a_ptr);
    
private:
    friend class singleton<ref_tracker>;
    ref_tracker();
    ~ref_tracker();
    ref_tracker(const ref_tracker& a_other) { }

    hash_table m_table;
};
    
}

#endif
