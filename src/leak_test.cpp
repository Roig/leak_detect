#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <vector>
// descomentar para probar operadores sobrecargados
#include "../include/file_line_new.h"

#define ARR_SIZE 50

void testfunc4(int* p1, int* p2)
{
    int* p_arr_test = new int[ARR_SIZE];
    int* p_arr_test_leak = new int[ARR_SIZE];

    for (unsigned int index = 0; index < ARR_SIZE; ++index)
    {
	p_arr_test[index] = *p1 + (rand() % 50);
	p_arr_test_leak[index] = *p2 + (rand() % 70);
    }

    delete[] p_arr_test;    
}

void testfunc3(int* p1, int* p2)
{
    testfunc4(p1, p2);
}

void testfunc2()
{
    int* p_test = new int;
    int* p_test_leak = new int;    

    *p_test = 5 + (rand() % 200);
    *p_test_leak = *p_test + 2;
    
    testfunc3(p_test, p_test_leak);

    delete p_test;    
}

void testfunc1()
{
    testfunc2();
}

int main(int argc, char* argv[])
{
    srand(time(NULL));    
    
    testfunc1();

    return 0;
}
