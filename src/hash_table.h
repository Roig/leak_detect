#ifndef HASH_TABLE_H_
#define HASH_TABLE_H_

#include "linked_list.h"

namespace leaks
{

class hash_table
{
public:    
    void init(unsigned int a_size);
    void clean();
    
    void* find(char* a_key);
    void insert(char* a_key, void* a_element);
    void* remove(char* a_key);

    unsigned int get_size();	// nro maximo de listas
    unsigned int get_count();	// nro de elementos en total (dentro de todas las listas)
    
    linked_list<str_list_element>* get_list(unsigned int a_index);

private:
    unsigned int hash(char* a_str);
    
    unsigned int m_size;
    unsigned int m_count;
    linked_list<str_list_element>** m_table;
};
    
}

#endif
